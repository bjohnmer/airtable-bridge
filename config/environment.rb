# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!
Rails.application.load_tasks

Rake::Task['import_copy_airtable:start'].invoke
