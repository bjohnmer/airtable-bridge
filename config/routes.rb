Rails.application.routes.draw do
  get 'copy/', to: 'copy#index'
  get 'copy/:key', to: 'copy#index', :constraints => { :key => /[0-z\.]+/ }
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
