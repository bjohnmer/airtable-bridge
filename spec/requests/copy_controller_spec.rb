require 'rails_helper'

RSpec.describe CopyController, type: :request do
  describe 'GET /copy/refresh' do
    it 'updates airtable json' do
      get copy_path(key: :refresh)

      expect(File).to exist("#{Rails.root}/public/copy.json")
      expect(response.body).to eq '{}'
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /copy/:key' do
    it 'returns the template' do
      get copy_path(key: 'test2', variable: 'example', nombre: 'John')

      expect(response.body).to eq '{"value":"Mi plantilla example John"}'
      expect(response).to have_http_status(200)
    end

    context 'when key does not exist' do
      it 'returns 404' do
        get copy_path(key: 'test3', variable: 'example', nombre: 'John')

        expect(response.body).to eq '{"error":"Not found"}'
        expect(response).to have_http_status(404)
      end
    end
  end

  describe 'GET /copy/since=timestamp' do
    it 'returns all templates since given timestamp' do
      get '/copy/since=1604638800'

      json = JSON.parse(response.body)
      date_before = json.any? { |h| h["fields"]["Last Modified"].to_date < Date.parse('11-06-2020') }
      expect(date_before).to be false
      expect(json.length).to eq 4
      expect(response).to have_http_status(200)
    end
  end
end
