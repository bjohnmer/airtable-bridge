require 'json'

desc 'Import Copy frm Airtable'
namespace :import_copy_airtable do
	task start: :environment do
    # Connect with airtable and get data request
    airtable_service = AirtableService.new
    body_req = airtable_service.get_request

    # Store the result in a json file
    airtable_service.store!(body_req[:records])
	end
end
