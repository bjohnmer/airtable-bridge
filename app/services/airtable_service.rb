class AirtableService
  AIRTABLE_URL = 'https://api.airtable.com/v0/app77wRWjrAVxx1M5/Table%201?view=Grid%20view'
  AIRTABLE_API_KEY = 'keyuQrm4cvc9745ZT'

  def get_request
    get_conn = connection.get do |req|
      req.headers['Authorization'] = "Bearer #{AIRTABLE_API_KEY}"
    end

    json_body = get_conn.body

    JSON.parse(json_body).with_indifferent_access
  end

  def store!(records)
    File.open(Rails.root.join('public', 'copy.json'),"w") do |f|
      f.write(records.to_json)
    end
  end

  private

  def connection
    Faraday.new(url: AIRTABLE_URL) do |faraday|
      faraday.request :url_encoded
      faraday.response :logger
      faraday.adapter Faraday.default_adapter
    end
  end
end
