class CopyController < ApplicationController
  before_action :validate_key

  def index
    render json: set_copy, status: :ok
  end

  private

  def validate_key
    @copy_file = copy_file
    return true if params[:key].include?('since') || params[:key] == 'refresh'

    row = copy_file.select { |el| el['fields']['Key'] == params[:key] }[0]

    not_found = row.nil?
    render(json: { error: 'Not found' }, status: :not_found) if not_found
  end

  def set_copy
    return @copy_file unless params[:key].present?

    return refresh if params[:key] == 'refresh'

    return since if params[:key].include?('since')

    find_by_key
  end

  def copy_file
    file_path = Rails.root.join('public', 'copy.json')

    data = ''
    File.open(file_path,"r") do |f|
      data = f.read
    end

    JSON.parse(data)
  end

  def refresh
    airtable_service = AirtableService.new
    body_req = airtable_service.get_request

    # Store the result in the json file
    airtable_service.store!(body_req[:records])

    {}
  end

  def find_by_key
    # Filters the by the kay param
    row = @copy_file.select { |el| el['fields']['Key'] == params[:key] }[0]

    # Gets all the replaceable vars from the Copy field
    # and removes the brackets
    all_in_brackets_re = Regexp.new('\{.*?\}')
    replaceable = row['fields']['Copy'].scan(all_in_brackets_re).map { |el| el.gsub(/{|}/, '') }

    # Builds an object that will help to replace all coincidences
    matchers = build_matchers(replaceable)

    # Replaces all coincidencies
    var_regex_list = Regexp.new(replaceable.join('|'))

    value = row['fields']['Copy'].gsub(var_regex_list) { |match| matchers[match] }.gsub(/{|}/, '')

    { value: value }
  end

  def since
    value = params[:key].split('=').last.to_i
    @copy_file.select { |el| DateTime.parse(el['fields']['Last Modified']) >= Time.at(value) }
  end
end
