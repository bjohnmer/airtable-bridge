class ApplicationController < ActionController::API

  private

  def build_matchers(replaceable)
    object = {}
    replaceable.each do |el|
      value = el.include?('datetime') ? to_datetime(params[el.split(',')[0]]) : params[el]
      object[el] = value
    end

    object
  end

  def to_datetime(el)
    Time.at(el.to_i).strftime("%a %b %d %I:%M:%S %p")
  end
end
